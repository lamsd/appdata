import pandas as pd 
from sklearn .model_selection import train_test_split
from category_encoders import OrdinalEncoder
from shapash.data.data_loader import data_loading
from config import FEATURE_DATA, TRAIN, FEATURE_DICT
import catboost

from shapash import SmartExplainer

def load_data(data):
    y = data["target"]
    X = data.drop(["target", "target_multi", "year_acc", "Description"], axis=1)
    features_to_encode = [
    col for col in X[FEATURE_DATA].columns if X[col].dtype not in ("float64", "int64")]
    encoder = OrdinalEncoder(cols=features_to_encode)
    encoder = encoder.fit(X[FEATURE_DATA])
    X_encoded = encoder.transform(X)
    return train_test_split(X_encoded, y, train_size=0.75, random_state=1), encoder

def train(Xtrain, ytrain, check_train=True):
    if check_train:
        model = catboost.CatBoostClassifier()
        model.fit(Xtrain, ytrain, verbose=True )
        model.save_model("modelA", format="cbm")
        print("Trained the database!")
    else:
        model = catboost.CatBoostClassifier()
        model = model.load_model("modelA", format="cbm")
        print("Start models")
    return model

def main(model, data, encoder, Xtest):
    xpl = SmartExplainer(
            model=model,
            preprocessing=encoder, # Optional: compile step can use inverse_transform method
            features_dict=FEATURE_DICT, # Optional: specifies label for features name
        ) 
    additional_data = data.loc[Xtest.index, ["year_acc", "Description"]]
    additional_features_dict = {"year_acc": "Year"}
    ypred=pd.DataFrame(model.predict(Xtest),columns=['pred'],index=Xtest.index)
    xpl.compile(
            x=Xtest, 
            y_pred=ypred, # Optional: for your own prediction (by default: model.predict)
            y_target=ytest, # Optional: allows to display True Values vs Predicted Values
            additional_data=additional_data, # Optional: additional dataset of features for Webapp
            additional_features_dict=additional_features_dict, # Optional: specifies label for additional features name
        )
    return xpl  
    

if __name__=="__main__":
    df_car_accident = data_loading("us_car_accident")
    (Xtrain, Xtest, ytrain, ytest), encoder = load_data(df_car_accident)
    model = train(Xtrain, ytrain, check_train=TRAIN["train"])
    xpl = main(model, df_car_accident, encoder, Xtest)
    xpl.run_app(host="192.168.133.129", port=8030)
