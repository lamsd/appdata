FEATURE_DATA = [
    "Start_Lat",
    "Start_Lng",
    "Distance(mi)",
    "Temperature(F)",
    "Humidity(%)",
    "Visibility(mi)",
    "day_of_week_acc",
    "Nautical_Twilight",
    "season_acc",
]
TRAIN = {
    "train": False
}

FEATURE_DICT = {
    "day_of_week_acc": "Day of week",
    "season_acc": "Season"
}
